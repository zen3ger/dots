set number
set relativenumber
set termguicolors

" Plugins
call plug#begin('~/.config/nvim/plugins')

" Code completion
" after plugin is installed, run the plugin dir install script
" ./install.py --rust-completer --clang-completer
Plug 'Valloric/YouCompleteMe'

" Rust stuff
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

" Theme
Plug 'ayu-theme/ayu-vim'

" Terminal
Plug 'kassio/neoterm'

call plug#end()

" Select dark variant of the theme
let ayucolor="dark"
colorscheme ayu

" Disable terminal background color override
highlight Normal ctermbg=none
highlight Normal guibg=none

" Code completion for rust
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1

" Rust configuration
let g:rustfmt_autosave = 1

nnoremap <leader>c :!cargo clippy

" Requires pynvim to be installed
let os = substitute(system('uname'), "\n", "", "")
if os == "FreeBSD"
	let g:python3_host_prog='/usr/local/bin/python3.7'
elseif os == "Linux"
	let g:python3_host_prog='/usr/bin/python3'
endif
