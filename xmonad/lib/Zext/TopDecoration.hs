{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Zext.TopDecoration
  ( TopDecoration (..),
    ShrinkToNothing,
    shrinkToNothing,
  )
where

import XMonad
import XMonad.Layout.Decoration
import qualified XMonad.StackSet as W
import XMonad.Util.Types

data TopDecoration a = TopDecoration
  deriving (Show, Read)

instance (Eq a) => DecorationStyle TopDecoration a where
  shrink _ (Rectangle _ _ dw dh) (Rectangle x y w h) = Rectangle x (y + fi dh) w (h - dh)

  pureDecoration _ dw dh _ st _ (win, Rectangle x y w h)
    | win `elem` W.integrate st && dw < w && dh < h = Just (Rectangle x y w dh)
    | otherwise = Nothing

--
-- Simple shrinker that removes the title from the decorator
--
data ShrinkToNothing = ShrinkToNothing
  deriving (Show, Read)

instance Shrinker ShrinkToNothing where
  shrinkIt _ _ = [""]

shrinkToNothing :: ShrinkToNothing
shrinkToNothing = ShrinkToNothing
