--
-- Imports
--
import XMonad
import XMonad.Actions.SinkAll (sinkAll)
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, defaultPP, dzenColor, shorten, wrap, PP(..))
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Decoration
import XMonad.Layout.Grid
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Tabbed
import XMonad.Prompt
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Window (allWindows, windowPrompt, WindowPrompt(..))
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.Run
import XMonad.Util.SpawnOnce (spawnOnce)
import qualified XMonad.StackSet as W

-- lib imports
import Zext.TopDecoration

-- std imports
import Data.Char (toLower)
import Data.List (isPrefixOf, isInfixOf, isSuffixOf)
import Data.Monoid
import Data.Time ( getCurrentTime
                 , getCurrentTimeZone
                 , defaultTimeLocale
                 , formatTime
                 , utcToLocalTime
                 )
import System.Exit
import qualified Data.Map as M

--
-- Variables
--
zPasswdMgr  = "keepassxc"
zWebBrowser = "firefox"
zTerm       = "alacritty"
zTextEditor = "emacs"
zBar        = "dzen2 -dock -ta c -fn 'Ubuntu Mono derivative Powerline:bold:size=11'"
zModMask    = mod4Mask

zFollowsMouse :: Bool
zFollowsMouse = True

zBorderWidth       = 3
zNormalBorderColor = "#f0f0f0"
zFocusBorderColor  = "#f95a04"

--
-- Util
--

zSafeRunInTerm (cmd : args) = safeSpawn zTerm ("-t" : cmd : "-e" : cmd : args)
zSafeRunInTerm _ = spawn zTerm

--
-- Workspaces
--
zWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

--
-- Keybindings
--

zKeys :: XConfig Layout -> M.Map (ButtonMask, KeySym) (X ())
zKeys = \conf -> mkKeymap conf $
  -- launch terminal
  [ ("M-<Return>", spawn $ XMonad.terminal conf)
  -- launch prompt
  , ("M-p", shellPrompt zPromptConfig)
  -- window prompt, goto window
  , ("M-<Tab>", windowPrompt zWindowPromptConfig Goto allWindows)
  -- window prompt, bring window
  , ("M-S-<Tab>", windowPrompt zWindowPromptConfig BringToMaster allWindows)
  -- open freebsd audio mixer
  , ("M-a", zSafeRunInTerm ["mixertui"])
  -- open music player
  , ("M-z", zSafeRunInTerm ["cmus"])
  -- increase volume
  , ("<XF86AudioRaiseVolume>", spawn "mixer vol +5")
  -- decrease volume
  , ("<XF86AudioLowerVolume>", spawn "mixer vol -5")
  -- set volume to 0, mute
  , ("<XF86AudioMute>", spawn "mixer vol 0")
  -- set volume to 75, unmute
  , ("S-<XF86AudioMute>", spawn "mixer vol 75")
  -- open text editor
  , ("M-e", spawn zTextEditor)
  -- open midnight commander as filemanager
  , ("M-f", zSafeRunInTerm ["mc", "-u"])
  -- open password manager
  , ("M-S-p", spawn zPasswdMgr)
  -- open webbrowser
  , ("M-w", spawn zWebBrowser)
  -- take a screenshot
  , ("<Print>", spawn "scrot ${HOME}/pictures/screenshots/screen_%Y-%m-%d.png -d 1")
  -- turn off monitor
  , ("M-S-l", spawn "xset dpms force off")
  -- close focused window
  , ("M-S-q", kill)
  -- cycle layouts
  , ("M-<Space>", sendMessage NextLayout)
  -- reset layouts on the current workspace to default
  , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf)
  -- move focus to next window
  , ("M-j", windows W.focusDown)
  -- move focus to previous window
  , ("M-k", windows W.focusUp)
  -- move focus to master window
  , ("M-m", windows W.focusMaster)
  -- swap focused window with master
  , ("M-S-m", windows W.swapMaster)
  -- swap focused window with next
  , ("M-S-j", windows W.swapDown)
  -- swap focused window with previous
  , ("M-S-k", windows W.swapUp)
  -- decrease master size
  , ("M-h", sendMessage Shrink)
  -- increase master size
  , ("M-l", sendMessage Expand)
  -- push window to tiling mode
  , ("M-t", withFocused $ windows . W.sink)
  -- push all windows to tiling mode
  , ("M-S-t", sinkAll)
  -- restart XMonad
  , ("M-S-r", spawn "xmonad --recompile && killall dzen2; xmonad --restart")
  -- kill XMonad
  , ("M-S-c", io (exitWith ExitSuccess))
  ]
  ++
  -- switch workspaces M-<n>
  -- move windows to workspace M-S-<n>
  [ ("M" ++ s ++ n, windows $ f i)
    | (i, n) <- zip zWorkspaces $ map (('-':) . show) $ [1..9]
    , (f, s) <- [(W.greedyView, ""), (W.shift, "-S")]
  ]

--
-- Mouse actions
--
zMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
  -- left-click, place window in floating mode and drag
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))
  -- middle-click, place window to the top of the stack
  , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
  -- right-click, place window in floating mode and resize
  , ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
  ]

--
-- Window decoration
--
zFocusDecorColor = zFocusBorderColor
zNormalDecorColor = zNormalBorderColor

zTheme :: Theme
zTheme = def
  { activeColor         = zFocusDecorColor
  , activeBorderColor   = zFocusDecorColor
  , inactiveColor       = zNormalDecorColor
  , inactiveBorderColor = zNormalDecorColor
  , decoWidth           = 4
  , decoHeight          = 4
  }

zDecorate :: Eq a => l a -> ModifiedLayout (Decoration TopDecoration ShrinkToNothing) l a
zDecorate = decoration shrinkToNothing zTheme TopDecoration

--
-- Layouts
--
zSpaced t b r l = spacingRaw True border True border True
  where
    border = Border t b r l

zFancyLayout l = zDecorate $ avoidStruts $ zSpaced 8 8 8 8 $ l

zLayout = zFancyLayout (threeCol ||| vsplit ||| Grid) ||| tabs ||| Full
  where
    master   = 1
    ratio    = 1/2
    delta    = 3/100
    vsplit   = Mirror (Tall master delta ratio)
    threeCol = ThreeColMid master delta ratio
    tabs     = avoidStruts $ tabbed shrinkToNothing zTheme

--
-- Autostart hooks
--
zStartupHook = do
  spawnOnce "nitrogen --restore &"
  spawnOnce "compton --no-fading-openclose &"

--
-- Log hook
--

zGetTime :: IO String
zGetTime = do
  t <- getCurrentTime
  z <- getCurrentTimeZone
  return $ formatTime defaultTimeLocale fmt $ utcToLocalTime z t
  where
    --fmt = " %Y.%m.%d [%H:%M] "
    fmt = " [%H:%M] "

zDatePP :: X (Maybe String)
zDatePP = io $ Just . zHighlightPP <$> zGetTime

zMarkPP m c = dzenColor c "" . wrap m ""

zHighlightPP = dzenColor "#c5e552" ""

zLayoutPP :: String -> String
zLayoutPP l
  | isSuffixOf "Full"        l = pp " [FULL] "
  | isSuffixOf "Mirror Tall" l = pp " [BOTT] "
  | isSuffixOf "Grid"        l = pp " [GRID] "
  | isSuffixOf "ThreeCol"    l = pp " [3COL] "
  | isInfixOf  "Tabbed"      l = pp " [TABS] "
  | otherwise                  = pp (" <" ++ l ++ "> ")
  where
    pp = zHighlightPP

zLogHook = \proc -> dynamicLogWithPP def
  { ppOutput           = \x -> hPutStrLn proc x
  , ppCurrent          = zMarkPP "°" zFocusDecorColor
  , ppVisible          = zMarkPP "*" zNormalBorderColor
  , ppVisibleNoWindows = Just (dzenColor zFocusBorderColor "")
  , ppHidden           = zMarkPP "°" zNormalBorderColor
  , ppHiddenNoWindows  = zMarkPP " " zNormalBorderColor
  , ppTitle            = dzenColor zFocusBorderColor "" . shorten 40
  , ppLayout           = zLayoutPP
  , ppSep              = ""
  , ppUrgent           = zMarkPP "!" "#c5e552"
  , ppExtras           = [zDatePP]
  , ppOrder            = \(ws:l:_:ex) -> [l] ++ [wrap "  [" "]  " ws] ++ ex
  }

--
-- Prompt
--
zPromptConfig = def
  { font              = "xft:Ubuntu:size=12"
  , bgColor           = "#1c1c1c"
  , fgColor           = "#d0d0d0"
  , bgHLight          = zFocusBorderColor
  , fgHLight          = "#1c1c1c"
  , borderColor       = "#1c1c1c"
  , promptBorderWidth = zBorderWidth
  , position          = CenteredAt { xpCenterY = 0.45, xpWidth = 0.35 }
  , height            = 20
  , historySize       = 256
  , defaultText       = []
  , autoComplete      = Nothing --Just 300000 -- in microsec, 0.3s
  , searchPredicate   = isPrefixOf
  , alwaysHighlight   = True
  , maxComplRows      = Just 15
  }

zWindowPromptConfig = zPromptConfig { searchPredicate = searchInfix }
  where
    searchInfix input maybeMatch = isInfixOf (map toLower input) (map toLower maybeMatch)

--
-- Main
--
main = do
  barproc <- spawnPipe zBar
  xmonad $ docks def
    { terminal           = zTerm
    , keys               = zKeys
    , startupHook        = zStartupHook
    , focusFollowsMouse  = zFollowsMouse
    , borderWidth        = 0
    , modMask            = zModMask
    , workspaces         = zWorkspaces
    , normalBorderColor  = zNormalBorderColor
    , focusedBorderColor = zFocusBorderColor
    , mouseBindings      = zMouseBindings
    , layoutHook         = zLayout
    , logHook            = zLogHook barproc
    }
